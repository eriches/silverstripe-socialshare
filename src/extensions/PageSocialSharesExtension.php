<?php

namespace Hestec\SocialShare;

use SilverStripe\Core\Extension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextareaField;

class PageSocialShareExtension extends Extension {

    private static $db = array(
        'SocialEnabled' => 'Boolean',
        'SocialPost' => 'Text',
        'SocialLastSelected' => 'Datetime'
    );

    public function updateCMSFields(FieldList $fields) {

        $SocialEnabledField = CheckboxField::create('SocialEnabled', "SocialEnabled");
        $SocialPostField = TextareaField::create('SocialPost', "SocialPost");
        $SocialPostField->setAttribute('data-fieldcounter', 255);

        $fields->addFieldsToTab("Root.SocialShare", array(
            $SocialEnabledField,
            $SocialPostField
        ));

    }

}
