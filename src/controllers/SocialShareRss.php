<?php

namespace Hestec\SocialShare;

use SilverStripe\Control\RSS\RSSFeed;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Config;

class SocialShareRss extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'rss',
        'cron'
    );

    public function init() {
        parent::init();

        /*if (!Member::currentUser()){
            return $this->httpError(404);
        }*/

    }

    public function rss(){

        $output = \Page::get()->filter(array('SocialEnabled' => true, 'SocialLastSelected:not' => NULL))->sort('SocialLastSelected DESC')->limit(1);

        $rss = new RSSFeed(
            $output,
            Director::absoluteBaseUrl(),
            "SocialShare",
            "SocialShare feed"
        );

        $rss->setTemplate('SocialShareRss');

        return $rss->outputToBrowser();

    }

    public function cron(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $currentdatetime = new \DateTime();

            $count = \Page::get()->filter(array('SocialEnabled' => true))->count();

            $limit = 5;
            if ($count <= $limit) {
                $limit = $count - 1;
            }

            $pages = \Page::get()->filter(array('SocialEnabled' => true))->sort('SocialLastSelected')->limit($limit)->toArray();

            if (!empty($pages)) {

                foreach ($pages[array_rand($pages)] as $node) {

                    $node->SocialLastSelected = $currentdatetime->format('Y-m-d H:i:s');
                    $node->write();
                    return "selected: " . $node->Title;

                }
            }
            return "no pages found";

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

}
